<?php

namespace vendor\nkaajtooj\module\src\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MakeModuleCommand
{
    protected static $defaultName = 'make:module';
    protected static $defaultDescription = 'Make redis-queue consumer';

    protected function configure()
    {
        $this->addArgument('name', InputArgument::REQUIRED, 'Consumer name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $name = $input->getArgument('name');
        $output->writeln("Make consumer $name");
    }
}
